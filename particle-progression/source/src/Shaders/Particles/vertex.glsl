// attribute float aParticleIndex;

uniform sampler2D uBasePosLookup;
uniform float uTime;

varying vec3 vPos;

void main() {
  vec4 pxData = texture2D(uBasePosLookup, position.xy);
  vPos = pxData.xyz;
  float randomness = pxData.a;
  vec4 modelPosition = modelMatrix * vec4(vPos, 1.0);
  vec4 viewPosition = viewMatrix * modelPosition;
  vec4 projectionPosition = projectionMatrix * viewPosition;

  gl_Position = projectionPosition;
  // gl_PointSize = (randomness + .2) * (10.0 / -viewPosition.z);
  gl_PointSize = (4.0 / -viewPosition.z);
  // gl_PointSize = 80.;
}