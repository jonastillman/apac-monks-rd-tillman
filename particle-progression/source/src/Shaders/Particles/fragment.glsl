uniform sampler2D uParticleTex;
uniform float uTime;

varying vec3 vPos;

void main() {
  // gl_FragColor = vec4(gl_PointCoord, 1.0, 1.0);
  vec4 color1 = vec4(0.7, 0.09, 0.09, 0.95);
  vec4 color2 = vec4(0.63, 0.7, 0.09, 0.95);
  vec4 color = mix(color1, color2, vPos.x);
  color *= texture2D(uParticleTex, gl_PointCoord);
  gl_FragColor = color;
  // gl_FragColor = texture2D(uParticleTex, gl_PointCoord);
}