import * as THREE from "three";
import GlEnv from "../GlEnv";
import ParticleModel from "./ParticleModel";

export default class Shanghai extends ParticleModel {
  scene: THREE.Scene;
  constructor(env: GlEnv, model: THREE.Mesh) {
    super(model, 2);
    this.scene = env.scene;
    this.scene.add(this.points);
  }

  // update() {}
}
