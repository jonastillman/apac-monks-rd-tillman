import * as THREE from "three";
import GlEnv from "../GlEnv";
import ParticleModel from "./ParticleModel";

export default class Hat extends ParticleModel {
  scene: THREE.Scene;
  constructor(env: GlEnv, model: THREE.Mesh) {
    super(model, 0.9);
    this.scene = env.scene;
    this.scene.add(this.points);
  }

  // update() {}
}
