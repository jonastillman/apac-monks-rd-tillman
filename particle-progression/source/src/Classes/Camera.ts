import * as THREE from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls.js";
import GlEnv from "../GlEnv";

export default class Camera {
  sizes: import("/Users/user/Dev/apac-monks-rd-tillman/particle-progression/source/src/Utils/SizeDomain").default;
  scene: THREE.Scene;
  canvas: HTMLCanvasElement;
  instance!: THREE.PerspectiveCamera;
  controls!: OrbitControls;
  constructor(env: GlEnv) {
    this.sizes = env.sizeDomain;
    this.scene = env.scene;
    this.canvas = env.renderer.instance.domElement;

    this.initCamera();
    this.setControls();
  }

  initCamera() {
    this.instance = new THREE.PerspectiveCamera(
      70,
      this.sizes.width / this.sizes.height,
      0.1,
      10
    );
    this.instance.position.set(2.5, 0.4, 0);
    this.scene.add(this.instance);
  }

  setControls() {
    this.controls = new OrbitControls(this.instance, this.canvas);
    this.controls.enableDamping = true;
  }

  resize() {
    this.instance.aspect = this.sizes.width / this.sizes.height;
    this.instance.updateProjectionMatrix();
  }

  update() {
    this.controls.update();
  }
}
