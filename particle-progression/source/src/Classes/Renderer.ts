import * as THREE from "three";
import GlEnv from "../GlEnv";

export default class Renderer {
  private sizes: import("/Users/user/Dev/apac-monks-rd-tillman/particle-progression/source/src/Utils/SizeDomain").default;
  private scene: THREE.Scene;
  private camera: import("/Users/user/Dev/apac-monks-rd-tillman/particle-progression/source/src/Classes/Camera").default;
  instance: THREE.WebGLRenderer;

  constructor(env: GlEnv) {
    this.sizes = env.sizeDomain;
    this.scene = env.scene;
    this.camera = env.camera;
    this.instance = new THREE.WebGLRenderer({
      antialias: true,
    });
    this.init();
  }

  init() {
    this.instance.physicallyCorrectLights = true;
    this.instance.outputEncoding = THREE.sRGBEncoding;
    // this.instance.toneMapping = THREE.CineonToneMapping;
    // this.instance.toneMappingExposure = 1.75;
    // this.instance.shadowMap.enabled = true;
    // this.instance.shadowMap.type = THREE.PCFSoftShadowMap;
    this.instance.setClearColor("#222");
    this.instance.setSize(this.sizes.width, this.sizes.height);
    this.instance.setPixelRatio(Math.min(this.sizes.pixelRatio, 2));
    document.body.appendChild(this.instance.domElement);
  }

  resize() {
    this.instance.setSize(this.sizes.width, this.sizes.height);
    this.instance.setPixelRatio(Math.min(this.sizes.pixelRatio, 2));
  }

  update() {
    this.instance.render(this.scene, this.camera.instance);
  }
}
