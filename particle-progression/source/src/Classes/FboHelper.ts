/*
this.position = new FBO({
  width: 128,
  height: 128,
  name: 'position',
  shader: require('./position.frag'),
  uniforms: {
    uTime: {
      value: 0
    },
  },
});

this.position.target
this.position.update()
*/
import * as THREE from "three";
import {
  WebGLRenderTarget,
  NearestFilter,
  DataTexture,
  RGBAFormat,
  FloatType,
  Camera,
  Scene,
  Mesh,
  PlaneBufferGeometry,
  MeshBasicMaterial,
  ShaderMaterial,
  Texture,
} from "three";
import GlEnv from "../GlEnv";

// export const isAvailable = (() => {
//   const gl = renderer.getContext();

//   if (!gl.getExtension("OES_texture_float")) {
//     return false;
//   }

//   if (gl.getParameter(gl.MAX_VERTEX_TEXTURE_IMAGE_UNITS) === 0) {
//     return false;
//   }

//   return true;
// })();

// TODO: rename as usage changing...
export default class FboHelper {
  options: any;
  renderer: any;
  camera: Camera;
  scene: Scene;
  index: number;
  copyData: boolean;
  textureList: DataTexture[];
  rt: WebGLRenderTarget[];
  material: any;
  mesh: Mesh<any, any>;
  debugGeometry!: PlaneBufferGeometry;
  debugMaterial!: MeshBasicMaterial;
  debugMesh!: Mesh<any, any>;

  constructor({
    width,
    height,
    data,
    shader,
    uniforms = {},
    debug = false,
  }: {
    width: number;
    height: number;
    data: Float32Array[];
    shader?: string;
    uniforms?: {
      [key: string]: { value: any };
    };
    // rtOptions: Object;
    debug: boolean;
  }) {
    this.options = arguments[0];
    this.renderer = GlEnv.getInstance().renderer;
    this.camera = new Camera();
    this.scene = new Scene();
    this.index = 0;
    this.copyData = false;
    this.textureList = data.map((data) => {
      return new DataTexture(data, width, height, RGBAFormat, FloatType);
    });
    console.log(this.textureList);
    this.textureList.forEach((tex) => (tex.needsUpdate = true));

    this.rt = [this.createRT(), this.createRT()];

    this.material = new ShaderMaterial({
      defines: {
        RESOLUTION: `vec2(${width.toFixed(1)}, ${height.toFixed(1)})`,
        NUM_TEXTURES: this.textureList.length,
      },
      uniforms: {
        ...uniforms,
        uTextureList: {
          value: this.textureList,
        },
        uTime: {
          value: GlEnv.getInstance().time,
        },
        currStartModel: {
          value: 0,
        },
        // currTargetModel: {
        //   value: 1,
        // },
      },
      vertexShader: `
        void main() {
          gl_Position = vec4(position, 1.0);
        }
      `,
      fragmentShader:
        shader ||
        `
        varying vec2 vUv;
        uniform float uTime;
        uniform sampler2D uTextureList[NUM_TEXTURES];
        uniform int currStartModel;
        // uniform int currTargetModel;

        void main() {
          vec2 uv = gl_FragCoord.xy / RESOLUTION.xy;
          vec4 startPos;
          vec4 endPos;
          if (currStartModel == 0) {
            startPos = texture2D(uTextureList[0], uv);
            endPos = texture2D(uTextureList[1], uv);
          } else if (currStartModel == 1) {
             startPos = texture2D(uTextureList[1], uv);
             endPos = texture2D(uTextureList[2], uv);
            } else if (currStartModel == 2) {
            startPos = texture2D(uTextureList[2], uv);
            endPos = texture2D(uTextureList[0], uv);
          } 
          // TODO: have if-elses populated programmatically (can't index in without constant)
          vec4 currPos = mix(startPos, endPos, pow(fract(uTime* 0.2), 2.));
          gl_FragColor = currPos;
        }
      `,
    });

    this.mesh = new Mesh(new PlaneBufferGeometry(2, 2), this.material);
    this.scene.add(this.mesh);

    if (debug) {
      this.initDebug();
    }

    // Question: How to animate?
    // Alternatives:
    // - GSAP
    // Update function?
    // Make use of uTime in shader
    // e.g.
    // #define TIME_PER_MODEL 3 (populated from outside constant) -> To loop => some modulo operation
    // float progId (Based on TIME_PER_MODEL and uTime, floor());
    // float modelProgression [0, 1] -> fract() version of the same variable that is floored to get progression
    // const loop = gsap.timeline();
    // loop.add();
    setInterval(() => {
      this.material.uniforms.currStartModel.value =
        ++this.material.uniforms.currStartModel.value % this.textureList.length;
    }, 5000);
  }

  initDebug() {
    this.debugGeometry = new PlaneBufferGeometry(2, 2);
    this.debugMaterial = new MeshBasicMaterial({
      map: this.target,
    });
    this.debugMesh = new Mesh(this.debugGeometry, this.debugMaterial);
    this.debugMesh.rotation.set(0, Math.PI / 2, 0);
    this.debugMesh.position.set(-2, 0, 0);
    GlEnv.getInstance().scene.add(this.debugMesh);
  }

  createRT() {
    return new WebGLRenderTarget(this.options.width, this.options.height, {
      minFilter: NearestFilter,
      magFilter: NearestFilter,
      stencilBuffer: false,
      depthBuffer: false,
      type: FloatType,
    });
  }

  get target() {
    return this.rt[this.index].texture;
  }

  get uniforms() {
    return this.material.uniforms;
  }

  update(switchBack = true) {
    const destIndex = this.index === 0 ? 1 : 0;
    const dest = this.rt[destIndex];

    this.material.uniforms.uTime.value = GlEnv.getInstance().time;
    const r = this.renderer.instance;
    const oldMainTarget = r.getRenderTarget();
    r.setRenderTarget(dest);
    r.render(this.scene, this.camera);
    switchBack && r.setRenderTarget(oldMainTarget);

    this.index = destIndex;
    this.copyData = false;
  }
}
