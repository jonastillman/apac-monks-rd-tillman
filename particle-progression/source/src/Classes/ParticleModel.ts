import * as THREE from "three";
import { MeshSurfaceSampler } from "three/examples/jsm/math/MeshSurfaceSampler";
import GlEnv from "../GlEnv";
import vertexShader from "../Shaders/Particles/vertex.glsl";
import fragmentShader from "../Shaders/Particles/fragment.glsl";
import FboHelper from "./FboHelper";

class SharedParticleMaterial {
  // glEnv: GlEnv;
  private static instance: THREE.ShaderMaterial | null = null;
  private static glEnv: GlEnv;
  private constructor() {
    SharedParticleMaterial.glEnv = GlEnv.getInstance();
    // console.log(SharedParticleMaterial.glEnv.modelLoader.particleTex);
  }

  public static getInstance() {
    if (!SharedParticleMaterial.instance) {
      new this();
      // SharedParticleMaterial.instance = new THREE.ShaderMaterial({
      //   vertexShader,
      //   fragmentShader,
      //   uniforms: {
      //     uParticleTex: {
      //       value: SharedParticleMaterial.glEnv.modelLoader.particleTex,
      //     },
      //     uTime: {
      //       value: SharedParticleMaterial.glEnv.time,
      //     },
      //   },
      //   transparent: true,
      //   blending: THREE.AdditiveBlending,
      //   depthWrite: false,
      // });
    }
    return SharedParticleMaterial.instance;
  }
}

const WIDTH = 512;
const HEIGHT = 512;
const MAX_PARTICLES = WIDTH * HEIGHT;

export default class ParticleModel {
  // models: THREE.Mesh<THREE.BufferGeometry, THREE.Material | THREE.Material[]>;
  points: THREE.Points<THREE.BufferGeometry, any>;
  pointsGeometry: THREE.BufferGeometry;
  material: THREE.ShaderMaterial;
  fbo: FboHelper;
  // particlesPerModel: number;

  constructor(models: { model: THREE.Mesh; scale: number }[]) {
    const tempPosition = new THREE.Vector3();
    // this.particlesPerModel = Math.floor(MAX_PARTICLES / models.length);
    const positionDataList: Float32Array[] = [];
    models.forEach(({ model, scale }) => {
      const posFull: Float32Array = new Float32Array(MAX_PARTICLES * 4);
      const sampler = new MeshSurfaceSampler(model).build();
      for (let i = 0; i < MAX_PARTICLES; i++) {
        const i4 = i * 4;
        sampler.sample(tempPosition);
        posFull[i4 + 0] = tempPosition.x * scale;
        posFull[i4 + 1] = tempPosition.y * scale;
        posFull[i4 + 2] = tempPosition.z * scale;
        posFull[i4 + 3] = Math.random();
      }
      positionDataList.push(posFull);
    });

    this.fbo = new FboHelper({
      width: WIDTH * 1,
      height: HEIGHT * 1,
      data: positionDataList,
      debug: false,
      // shader: `
      //   uniform sampler2D uTexture;

      //   void main() {
      //     vec2 uv = gl_FragCoord.xy / RESOLUTION.xy;
      //     gl_FragColor = texture2D(uTexture, uv);
      //   }
      // `,
    });

    this.material = new THREE.ShaderMaterial({
      vertexShader,
      fragmentShader,
      uniforms: {
        uParticleTex: {
          value: GlEnv.getInstance().modelLoader.particleTex,
        },
        uTime: {
          value: GlEnv.getInstance().time,
        },
        uBasePosLookup: {
          value: this.fbo.target,
        },
      },
      transparent: true,
      blending: THREE.AdditiveBlending,
      depthWrite: false,
    });

    var vertices = new Float32Array(MAX_PARTICLES * 3);
    for (var i = 0; i < MAX_PARTICLES; i++) {
      var i3 = i * 3;
      vertices[i3] = (i % WIDTH) / WIDTH;
      vertices[i3 + 1] = i / WIDTH / HEIGHT;
    }

    this.pointsGeometry = new THREE.BufferGeometry();
    this.pointsGeometry.setAttribute(
      "position",
      new THREE.BufferAttribute(vertices, 3)
    );

    this.points = new THREE.Points(this.pointsGeometry, this.material);

    GlEnv.getInstance().scene.add(this.points);
  }

  update() {
    this.material.uniforms.uTime.value = GlEnv.getInstance().time;
    this.fbo.update();
  }

  dispose() {
    this.pointsGeometry.dispose();
  }
}
