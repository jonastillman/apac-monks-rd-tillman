import { Events } from "../Data/Events";

type noReturn = (data: any) => void;

export default class EventEmitter {
  private callbacks: {
    [key in Events]?: noReturn[];
  };

  constructor() {
    this.callbacks = {};
  }

  on(event: Events, cb: noReturn) {
    if (!this.callbacks[event]) {
      this.callbacks[event] = [];
    }
    this.callbacks[event]!.push(cb);
  }

  emit(event: Events, data = {}) {
    let cbs = this.callbacks[event];
    if (cbs) {
      cbs.forEach((cb) => cb(data));
    }
  }
}
