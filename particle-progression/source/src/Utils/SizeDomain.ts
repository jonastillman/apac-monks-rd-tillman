import EventEmitter from "./eventEmitter";
import { Events } from "../Data/Events";

export default class SizeDomain extends EventEmitter {
  width: number;
  height: number;
  pixelRatio: number;
  constructor() {
    super();

    // Setup
    this.width = window.innerWidth;
    this.height = window.innerHeight;
    this.pixelRatio = Math.min(window.devicePixelRatio, 2);

    // Resize event
    window.addEventListener("resize", () => {
      this.width = window.innerWidth;
      this.height = window.innerHeight;
      this.pixelRatio = Math.min(window.devicePixelRatio, 2);

      this.emit(Events.RESIZE);
    });
  }
}
