import EventEmitter from "./eventEmitter";
import { LoadingManager, TextureLoader, sRGBEncoding } from "three";
import { Events } from "../Data/Events";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { DRACOLoader } from "three/examples/jsm/loaders/DRACOLoader.js";

export default class ModelLoader extends EventEmitter {
  particleTex!: THREE.Texture;
  models: any;

  constructor() {
    super();

    const loadingManager = new LoadingManager(() => {
      this.emit(Events.LOADED_ALL);
    });

    const texLoader = new TextureLoader(loadingManager);

    texLoader.load("./assets/circle_05.png", (texture) => {
      this.particleTex = texture;
      this.particleTex.encoding = sRGBEncoding;
    });

    // Instantiate a loader
    const dracoLoader = new DRACOLoader();
    dracoLoader.setDecoderPath("/draco/");

    const loader = new GLTFLoader(loadingManager);
    loader.setDRACOLoader(dracoLoader);

    loader.load("./assets/all_combined_compressed.glb", (geometry) => {
      this.emit(Events.LOADED_MODELS);
      this.models = geometry.scene.children;
    });
  }
}
