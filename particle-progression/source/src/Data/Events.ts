export enum Events {
  RESIZE = "resize",
  LOADED_MODELS = "loaded_models",
  LOADED_ALL = "loaded_all",
  LOADED_TEXTURES = "loaded_tex",
}
