import * as THREE from "three";
import Camera from "./Classes/Camera";
import Renderer from "./Classes/Renderer";
import SizeDomain from "./Utils/SizeDomain";
import ModelLoader from "./Utils/ModelLoader";
import { Events } from "./Data/Events";
import Arrow from "./Classes/Arrow";
import ParticleModel from "./Classes/ParticleModel";
import Hat from "./Classes/Hat";
import Shanghai from "./Classes/Shanghai";
import { debug } from "console";

export default class GlEnv {
  scene: THREE.Scene = new THREE.Scene();
  camera: Camera;
  renderer: Renderer;
  sizeDomain: SizeDomain;
  currentModel: number = 0;
  models: { model: THREE.Mesh; scale: number }[] = [];
  private globalTimer: THREE.Clock;

  private static instance: GlEnv | null = null;
  modelLoader: any;
  time: number = 0;
  particleModels!: ParticleModel;

  private constructor() {
    this.sizeDomain = new SizeDomain();
    this.renderer = new Renderer(this);
    this.camera = new Camera(this);

    this.sizeDomain.on(Events.RESIZE, () => this.resize());

    this.globalTimer = new THREE.Clock(false);
    this.modelLoader = new ModelLoader();
    this.modelLoader.on(Events.LOADED_ALL, () => {
      this.modelLoader.models.forEach((model: THREE.Mesh) => {
        switch (model.name) {
          case "Arrow":
            this.models[1] = { model, scale: 0.5 };
            break;
          case "Hat":
            this.models[0] = { model, scale: 0.9 };
            break;
          case "Shanghai":
            this.models[2] = { model, scale: 2 };
            break;

          default:
            break;
        }
      });
      this.particleModels = new ParticleModel(this.models);
    });
    this.modelLoader.on(Events.LOADED_ALL, () => {
      this.globalTimer.start();
      this.renderer.instance.setAnimationLoop(this.update.bind(this));
      // setInterval(() => {
      //   this.models[this.currentModel].points.visible = false;
      //   this.currentModel = ++this.currentModel % this.models.length;
      //   this.models[this.currentModel].points.visible = true;
      // }, 3000);
    });
  }

  public static getInstance(): GlEnv {
    if (!GlEnv.instance) {
      GlEnv.instance = new GlEnv();
    }

    return GlEnv.instance;
  }

  update() {
    this.renderer.instance.render(this.scene, this.camera.instance);
    this.time = this.globalTimer.getElapsedTime();
    this.particleModels.update();
    // this.models[this.currentModel].update();
  }

  resize() {
    this.camera.resize();
    this.renderer.resize();
  }
}
