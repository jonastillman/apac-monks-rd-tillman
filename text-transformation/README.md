# Text Transformations
### Summary ###

* "Get things up quickly"-kind of code to test multiple ways to do text transformations
* Folder 01-pre-processed-svgs--minimal
  * Using vector-processing to prepare SVGs already split up
* Folder 02-svg-filters--minimal
  * Similar as 01-folder, but a bit more meat, and using SVG-filters
  * Not implemented any resizing of screen and similar, will be severely broken on mobile
* Folder 03-displacement-profiling
  * Keep adding elements until below 40 FPS => alerting user
* [Scrapboard of workflow](https://docs.google.com/document/d/1C2rjMMeDbXh7VgktD5U7OWaKtNTPjfe3yw_sGfTFmMQ/edit#heading=h.gncq8v8ag0am)
* [Profiling and similar (folder 03-displacement-profiling)](https://docs.google.com/presentation/d/1QiygrWwuukwc3uiTqtimBcE-brkPjen4OXUmGsbCPuA/edit#slide=id.p)

### How do I get set up? ###

* Everything kept as scripty as possible, no modules
  * So if needed, could start up a local server, but should work to simply click the index.html in each folder
