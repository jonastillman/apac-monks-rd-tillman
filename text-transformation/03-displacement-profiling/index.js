(function () {
  const palette = ["#262254", "#1B7979", "#EC4176"];
  const svg = document.querySelector(".main-svg");
  const textContainer = document.querySelector(".text-container");
  const displacementMap = document.querySelector("#displacementMap");
  const displacementMapControl = document.querySelector(
    "#displacementMapControl"
  );
  const displacementElements = [];

  svg.setAttribute("viewBox", `0 0 ${window.innerWidth} ${window.innerHeight}`);

  // NEW

  // gsap.set(displacementMapControl, {
  //   attr: { scale: 200 },
  // });

  // const startValue = 600;
  // const endValue = 0;

  // // return a value between start and end as l goes from 0 to 1
  // function lerp(start, end, l) {
  //   return start + (end - start) * l;
  // }

  // const startTime = performance.now();

  // const durationInSeconds = 1;
  // function oscilate(now) {
  //   const timeSinceStartInSeconds = (now - startTime) * 0.001;

  //   // l goes from 0 to 1 over durationInSeconds;
  //   // const l = (timeSinceStartInSeconds / durationInSeconds) % 1;
  //   const l = (1 + Math.sin(timeSinceStartInSeconds / durationInSeconds)) * 0.5;
  //   displacementMap.setAttribute(
  //     "xlink:href",
  //     `data:image/svg+xml;charset=utf-8,${displacementSvgInterpolated(l)}`
  //   );

  //   requestAnimationFrame(oscilate);
  // }
  // requestAnimationFrame(oscilate);

  // END NEW

  // BACKUP
  displacementMap.setAttribute(
    "xlink:href",
    `data:image/svg+xml;charset=utf-8,${displacementSvg()}`
  );

  // Movement timeline
  const tl = gsap.timeline({
    repeat: -1,
    yoyo: true,
  });

  tl.to(
    displacementMapControl,
    {
      duration: 3,
      attr: { scale: 200 },
    },
    "end"
  );

  // END BACKUP

  addSvgText(textContainer, palette, displacementElements);
  setInterval(() => {
    addSvgText(textContainer, palette, displacementElements);
  }, 2000);

  fps(displacementElements);
})();

function constrain(n, low, high) {
  return Math.max(Math.min(n, high), low);
}

function addSvgText(container, palette, displacementElements) {
  var textElement = document.createElementNS(
    "http://www.w3.org/2000/svg",
    "text"
  );
  textElement.textContent = "DISPLACEMENT";
  textElement.setAttribute("x", 50 + (Math.random() - 0.5) * 50 + "%");
  textElement.setAttribute("y", 50 + (Math.random() - 0.5) * 50 + "%");
  textElement.setAttribute("dominant-baseline", "central");
  // textElement.setAttribute("font-size", "50");
  textElement.setAttribute("filter", "url(#displacementFilter)");
  textElement.style.fill = palette[Math.floor(Math.random() * palette.length)];
  container.appendChild(textElement);
  displacementElements.push(textElement);
}

function fps(displacementElementsRef) {
  let startTime = Date.now();
  let frame = 0;

  function tick() {
    const time = Date.now();
    frame++;
    if (time - startTime > 1000) {
      const fps = (frame / ((time - startTime) / 1000)).toFixed(1);
      // console.log("FPS", (frame / ((time - startTime) / 1000)).toFixed(1));
      if (fps < 40) {
        alert(
          `FPS dropped below 40, animated: ${displacementElementsRef.length}`
        );
        // console.log("FPS dropped below 40");
      }
      startTime = time;
      frame = 0;
    }
    window.requestAnimationFrame(tick);
  }
  tick();
}

function displacementSvg() {
  return encodeURIComponent(`<svg xmlns="http://www.w3.org/2000/svg" width="1000" height="600">\n
    <rect width="1000" height="100" fill="rgb(255, 0, 128)" />
    <rect width="1000" height="100" y="100" fill="rgb(242, 0, 128)"/>
    <rect width="1000" height="100" y="200" fill="rgb(230, 0, 128)"/>
    <rect width="1000" height="100" y="300" fill="rgb(216, 0, 128)"/>
    <rect width="1000" height="100" y="400" fill="rgb(204, 0, 128)"/>
    <rect width="1000" height="100" y="500" fill="rgb(191, 0, 128)"/>
  </svg>`);
}

// function displacementSvgInterpolated(dispFact) {
//   return encodeURIComponent(`<svg xmlns="http://www.w3.org/2000/svg" width="1000" height="600">\n
//     <rect width="1000" height="100" fill="rgb(${
//       128 + 127 * dispFact
//     }, 0, 128)" />
//     <rect width="1000" height="100" y="100" fill="rgb(${
//       128 + 117 * dispFact
//     }, 0, 128)"/>
//     <rect width="1000" height="100" y="200" fill="rgb(${
//       128 + 107 * dispFact
//     }, 0, 128)"/>
//     <rect width="1000" height="100" y="300" fill="rgb(${
//       128 + 90 * dispFact
//     }, 0, 128)"/>
//     <rect width="1000" height="100" y="400" fill="rgb(${
//       128 + 80 * dispFact
//     }, 0, 128)"/>
//     <rect width="1000" height="100" y="500" fill="rgb(${
//       128 + 70 * dispFact
//     }, 0, 128)"/>
//   </svg>`);
// }
