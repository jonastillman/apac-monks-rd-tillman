(function () {
  const view1 = document.querySelector(".main-text-container");
  const view2 = document.querySelector(".view-2");
  const textSvgContainer = document.querySelector(".main-text");
  const rotatedLetter = document.querySelector(".letter-c-rotated");
  const sideBar = document.querySelector(".outer-sidebar");
  const sideBarClipEl = document.querySelector("#cutoutSidebar");

  const clickTextGroup = document.querySelector(".click-text-container");
  const arrowGroup = document.querySelector(".letter-arrow");
  const cutoutCircle = document.querySelector("#cutoutCirlcle");
  const cutoutCircle2 = document.querySelector("#cutoutCirlcleView2");
  const blurFilter = document.querySelector("#blurDefinition");
  const view2Background = document.querySelector(
    ".background-image-container-2nd-view"
  );
  const displacementMap = document.querySelector("#displacementMap");
  const displacementMapControl = document.querySelector(
    "#displacementMapControl"
  );
  const displacementMapControl2 = document.querySelector(
    "#displacementMapControl2"
  );
  const approachTextGroup = document.querySelector("#approach-text-group");
  const directionTextGroup = document.querySelector("#direction-text-group");

  const introTimeline = gsap.timeline({ delay: 1 });

  // Intro animation
  gsap.set(clickTextGroup, { x: -1000 });
  gsap.set(arrowGroup, { x: -1000 });
  introTimeline
    .to(clickTextGroup, {
      duration: 1,
      x: 0,
    })
    .to(arrowGroup, {
      duration: 1.5,
      x: 0,
    })
    .to(blurFilter, {
      duration: 1.5,
      attr: { stdDeviation: 0 },
    });

  // 2nd viewClick timeline
  const clickTimeline2 = gsap
    .timeline({})
    .pause()
    .to(displacementMapControl2, {
      duration: 1,
      attr: { scale: 50 },
    });

  // Click animation
  const clickTimeline = gsap
    .timeline({
      onComplete: () => {
        view1.style = "display: none;";
        initMouseFollow(cutoutCircle2, directionTextGroup, clickTimeline2);
      },
    })
    .pause();
  clickTimeline
    .to(
      cutoutCircle,
      {
        cx: window.innerWidth / 2,
        cy: window.innerHeight / 2,
        r: window.innerHeight / 2 - 20,
        duration: 1.5,
      },
      "start"
    )
    .to(
      arrowGroup,
      {
        "font-size": "10vw",
        duration: 1,
      },
      "start"
    )
    .to(
      arrowGroup,
      {
        x: window.innerWidth,
        duration: 1.5,
      },
      "textShift"
    )
    .to(
      cutoutCircle,
      {
        r: 0,
        duration: 0.6,
        ease: "power3.in",
      },
      "textShift"
    )
    .to(
      displacementMapControl,
      {
        duration: 0.5,
        attr: { scale: 1000 },
      },
      "textShift"
    )
    .to(
      approachTextGroup,
      {
        duration: 1.5,
        x: -approachTextGroup.getBoundingClientRect().right * 3,
      },
      "textShift"
    )
    .to(clickTextGroup, {
      x: clickTextGroup.getBoundingClientRect().right,
    })
    .to(
      sideBar,
      {
        y: window.innerHeight,
      },
      "pageTransition"
    )
    .to(
      [view2Background, view2],
      {
        y: window.innerHeight,
      },
      "pageTransition"
    );

  initMouseFollow(cutoutCircle, approachTextGroup, clickTimeline);

  textSvgContainer.setAttribute(
    "viewBox",
    `0 0 ${window.innerWidth} ${window.innerHeight}`
  );
  document
    .querySelector(".view-2")
    .setAttribute("viewBox", `0 0 ${window.innerWidth} ${window.innerHeight}`);

  const rotatePivotX =
    parseInt(rotatedLetter.getAttribute("x")) * 0.01 * window.innerWidth;
  const rotatePivotY =
    parseInt(rotatedLetter.getAttribute("y")) * 0.01 * window.innerHeight;
  rotatedLetter.setAttribute(
    "transform",
    `rotate(-40, ${rotatePivotX}, ${rotatePivotY})`
  );

  displacementMap.setAttribute(
    "xlink:href",
    `data:image/svg+xml;charset=utf-8,${displacementSvg(30)}`
  );

  sideBarClipEl.setAttribute("width", sideBar.clientWidth);
  sideBarClipEl.setAttribute("height", window.innerHeight);
})();

function initMouseFollow(mouseFollowElement, mouseClickElement, clickTimeline) {
  let mouseX = -100,
    mouseY = -100;
  let xp = 0,
    yp = 0;

  document.addEventListener("mousemove", (e) => {
    mouseX = e.pageX;
    mouseY = e.pageY;
  });

  mouseClickElement.addEventListener("click", (e) => {
    clearInterval(interval);
    clickTimeline.play();
  });

  let interval = setInterval(function () {
    xp += (mouseX - xp) / 6;
    yp += (mouseY - yp) / 6;
    const centerDist = Math.sqrt(
      (mouseX - window.innerWidth / 2) ** 2 +
        (mouseY - window.innerHeight / 2) ** 2
    );
    mouseFollowElement.setAttribute("cx", xp + "px");
    mouseFollowElement.setAttribute("cy", yp + "px");
    mouseFollowElement.setAttribute(
      "r",
      map(Math.max(0, 0.5 - centerDist / window.innerWidth), 0, 0.5, 20, 300) +
        "px"
    );
  }, 20);
}

function map(n, start1, stop1, start2, stop2, withinBounds) {
  const newval = ((n - start1) / (stop1 - start1)) * (stop2 - start2) + start2;
  if (!withinBounds) {
    return newval;
  }
  if (start2 < stop2) {
    return this.constrain(newval, start2, stop2);
  } else {
    return this.constrain(newval, stop2, start2);
  }
}

function constrain(n, low, high) {
  return Math.max(Math.min(n, high), low);
}

// Creates static displacement map - to be animated through filter strength
function displacementSvg(numRows) {
  let gradientStops = "";
  const noXDisplacement = 128;
  for (let index = 0; index < numRows; index++) {
    const fractionOffset = index / numRows;
    const percentageOffset = fractionOffset * 100;
    const xRandomFactor = (Math.random() - 0.5) * 2 * 5;
    const stopColorR =
      255 + (noXDisplacement - 255) * fractionOffset + xRandomFactor;
    gradientStops += `<stop offset="${percentageOffset}%" stop-color="rgb(${stopColorR}, 0, 128)"></stop>`;
    if (percentageOffset < 100) {
      gradientStops += `<stop offset="${
        ((index + 1) / numRows) * 100
      }%" stop-color="rgb(${stopColorR}, 0, 128)"></stop>`;
    }
  }

  return encodeURIComponent(`
    <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100">
      <linearGradient id="offsetGradient" gradientTransform="rotate(90)">
        ${gradientStops}
      </linearGradient>
      <rect width="100" height="100" fill="url(#offsetGradient)"></rect>
  </svg>`);
}
