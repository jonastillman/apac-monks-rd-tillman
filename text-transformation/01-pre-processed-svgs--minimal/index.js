(function() {
  
  const approachElements = document.querySelectorAll(".text-row");
  const backgroundImageElement = document.querySelector(".background-image-container");
  const nestedSvgText = document.querySelector(".a-text-container svg");
  const arrowEl = document.querySelectorAll(".arrow");
  const half = Math.ceil(approachElements.length / 2);    
  const firstHalf = Array.prototype.slice.call(approachElements, 0, half)
  const secondHalf = Array.prototype.slice.call(approachElements, -half)

  const tl = gsap.timeline({
    delay: 2,
    repeat: -1,
    repeatDelay: 5, 
  });

  tl
    .to(secondHalf, {
      duration: 2,
      x: -window.innerWidth * 2,
      stagger: 0.05, 
      ease: "power1.out",
    }, 0)
    .to(firstHalf, {
      duration: 1.5,
      x: -window.innerWidth * 2,
      stagger: 0.04, 
      ease: "power1.out",
    }, 0.5)
    // .to(arrowEl, {
    //   x: window.innerWidth,
    //   duration: 1.5,
    //   ease: "expo.in",
    // }, 0.1)
    .set(approachElements, {
      x: window.innerWidth * 2,
    })
    // .set(arrowEl, {
    //   x: -window.innerWidth,
    // })
    .to(approachElements, {
      x: 0,
      duration: 1.5,
      stagger: 0.05,
    })
    // .to(arrowEl, {
    //   x: '-25%',
    //   duration: 2
    // })
    ;  


    nestedSvgText.setAttribute("viewBox", `0 0 ${backgroundImageElement.clientWidth} ${backgroundImageElement.clientHeight}`);
  
})();